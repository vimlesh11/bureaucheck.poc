/*
 * UnsecuredAPI
 *
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: v1
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */
using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace IO.Swagger.Models
{ 
    /// <summary>
    /// Complex type that holds the risk decision data.
    /// </summary>
    [DataContract]
    public partial class Decision : IEquatable<Decision>
    { 
        /// <summary>
        /// ReferenceId for the Product applied for.
        /// </summary>
        /// <value>ReferenceId for the Product applied for.</value>
        [DataMember(Name="ProductReferenceId")]
        public string ProductReferenceId { get; set; }

        /// <summary>
        /// Unique Decision Indentifier.
        /// </summary>
        /// <value>Unique Decision Indentifier.</value>
        [Required]
        [DataMember(Name="DecisionLogId")]
        public long? DecisionLogId { get; set; }

        /// <summary>
        /// The Risk Decision (e.g. A &#x3D; Accept, R &#x3D; Refer, D &#x3D; Decline, C &#x3D; Cancelled).
        /// </summary>
        /// <value>The Risk Decision (e.g. A &#x3D; Accept, R &#x3D; Refer, D &#x3D; Decline, C &#x3D; Cancelled).</value>
        [Required]
        [DataMember(Name="RiskDecision")]
        public string RiskDecision { get; set; }

        /// <summary>
        /// This is the primary risk decision reason code.
        /// </summary>
        /// <value>This is the primary risk decision reason code.</value>
        [Required]
        [DataMember(Name="PrimaryReasonCode")]
        public string PrimaryReasonCode { get; set; }

        /// <summary>
        /// A random digit generated to determine champion / challenger strategy. flows.
        /// </summary>
        /// <value>A random digit generated to determine champion / challenger strategy. flows.</value>
        [Required]
        [DataMember(Name="RandomDigit")]
        public int? RandomDigit { get; set; }

        /// <summary>
        /// A date / time stamp generated for the Risk Decision done.
        /// </summary>
        /// <value>A date / time stamp generated for the Risk Decision done.</value>
        [Required]
        [DataMember(Name="DecisionDateTime")]
        public DateTime? DecisionDateTime { get; set; }

        /// <summary>
        /// The Risk Band associated to the decision.
        /// </summary>
        /// <value>The Risk Band associated to the decision.</value>
        [Required]
        [DataMember(Name="RiskBand")]
        public int? RiskBand { get; set; }

        /// <summary>
        /// TBD
        /// </summary>
        /// <value>TBD</value>
        [DataMember(Name="RiskTier")]
        public int? RiskTier { get; set; }

        /// <summary>
        /// TBD
        /// </summary>
        /// <value>TBD</value>
        [DataMember(Name="FacilityType")]
        public string FacilityType { get; set; }

        /// <summary>
        /// Gets or Sets DecisionDetail
        /// </summary>
        [Required]
        [DataMember(Name="DecisionDetail")]
        public DecisionDetail DecisionDetail { get; set; }

        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class Decision {\n");
            sb.Append("  ProductReferenceId: ").Append(ProductReferenceId).Append("\n");
            sb.Append("  DecisionLogId: ").Append(DecisionLogId).Append("\n");
            sb.Append("  RiskDecision: ").Append(RiskDecision).Append("\n");
            sb.Append("  PrimaryReasonCode: ").Append(PrimaryReasonCode).Append("\n");
            sb.Append("  RandomDigit: ").Append(RandomDigit).Append("\n");
            sb.Append("  DecisionDateTime: ").Append(DecisionDateTime).Append("\n");
            sb.Append("  RiskBand: ").Append(RiskBand).Append("\n");
            sb.Append("  RiskTier: ").Append(RiskTier).Append("\n");
            sb.Append("  FacilityType: ").Append(FacilityType).Append("\n");
            sb.Append("  DecisionDetail: ").Append(DecisionDetail).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }

        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

        /// <summary>
        /// Returns true if objects are equal
        /// </summary>
        /// <param name="obj">Object to be compared</param>
        /// <returns>Boolean</returns>
        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            return obj.GetType() == GetType() && Equals((Decision)obj);
        }

        /// <summary>
        /// Returns true if Decision instances are equal
        /// </summary>
        /// <param name="other">Instance of Decision to be compared</param>
        /// <returns>Boolean</returns>
        public bool Equals(Decision other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;

            return 
                (
                    ProductReferenceId == other.ProductReferenceId ||
                    ProductReferenceId != null &&
                    ProductReferenceId.Equals(other.ProductReferenceId)
                ) && 
                (
                    DecisionLogId == other.DecisionLogId ||
                    DecisionLogId != null &&
                    DecisionLogId.Equals(other.DecisionLogId)
                ) && 
                (
                    RiskDecision == other.RiskDecision ||
                    RiskDecision != null &&
                    RiskDecision.Equals(other.RiskDecision)
                ) && 
                (
                    PrimaryReasonCode == other.PrimaryReasonCode ||
                    PrimaryReasonCode != null &&
                    PrimaryReasonCode.Equals(other.PrimaryReasonCode)
                ) && 
                (
                    RandomDigit == other.RandomDigit ||
                    RandomDigit != null &&
                    RandomDigit.Equals(other.RandomDigit)
                ) && 
                (
                    DecisionDateTime == other.DecisionDateTime ||
                    DecisionDateTime != null &&
                    DecisionDateTime.Equals(other.DecisionDateTime)
                ) && 
                (
                    RiskBand == other.RiskBand ||
                    RiskBand != null &&
                    RiskBand.Equals(other.RiskBand)
                ) && 
                (
                    RiskTier == other.RiskTier ||
                    RiskTier != null &&
                    RiskTier.Equals(other.RiskTier)
                ) && 
                (
                    FacilityType == other.FacilityType ||
                    FacilityType != null &&
                    FacilityType.Equals(other.FacilityType)
                ) && 
                (
                    DecisionDetail == other.DecisionDetail ||
                    DecisionDetail != null &&
                    DecisionDetail.Equals(other.DecisionDetail)
                );
        }

        /// <summary>
        /// Gets the hash code
        /// </summary>
        /// <returns>Hash code</returns>
        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                var hashCode = 41;
                // Suitable nullity checks etc, of course :)
                    if (ProductReferenceId != null)
                    hashCode = hashCode * 59 + ProductReferenceId.GetHashCode();
                    if (DecisionLogId != null)
                    hashCode = hashCode * 59 + DecisionLogId.GetHashCode();
                    if (RiskDecision != null)
                    hashCode = hashCode * 59 + RiskDecision.GetHashCode();
                    if (PrimaryReasonCode != null)
                    hashCode = hashCode * 59 + PrimaryReasonCode.GetHashCode();
                    if (RandomDigit != null)
                    hashCode = hashCode * 59 + RandomDigit.GetHashCode();
                    if (DecisionDateTime != null)
                    hashCode = hashCode * 59 + DecisionDateTime.GetHashCode();
                    if (RiskBand != null)
                    hashCode = hashCode * 59 + RiskBand.GetHashCode();
                    if (RiskTier != null)
                    hashCode = hashCode * 59 + RiskTier.GetHashCode();
                    if (FacilityType != null)
                    hashCode = hashCode * 59 + FacilityType.GetHashCode();
                    if (DecisionDetail != null)
                    hashCode = hashCode * 59 + DecisionDetail.GetHashCode();
                return hashCode;
            }
        }

        #region Operators
        #pragma warning disable 1591

        public static bool operator ==(Decision left, Decision right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(Decision left, Decision right)
        {
            return !Equals(left, right);
        }

        #pragma warning restore 1591
        #endregion Operators
    }
}
