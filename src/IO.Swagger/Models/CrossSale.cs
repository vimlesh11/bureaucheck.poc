/*
 * UnsecuredAPI
 *
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: v1
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */
using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace IO.Swagger.Models
{ 
    /// <summary>
    /// Complex type that holds cross sale related data.
    /// </summary>
    [DataContract]
    public partial class CrossSale : IEquatable<CrossSale>
    { 
        /// <summary>
        /// Product being cross sold.
        /// </summary>
        /// <value>Product being cross sold.</value>
        [Required]
        [DataMember(Name="Product")]
        public string Product { get; set; }

        /// <summary>
        /// Date the cross sale offer expires.
        /// </summary>
        /// <value>Date the cross sale offer expires.</value>
        [Required]
        [DataMember(Name="ExpiryDate")]
        public DateTime? ExpiryDate { get; set; }

        /// <summary>
        /// The cross sale amount or limit.
        /// </summary>
        /// <value>The cross sale amount or limit.</value>
        [Required]
        [DataMember(Name="Amount")]
        public double? Amount { get; set; }

        /// <summary>
        /// The cross sale APR (where applicable).
        /// </summary>
        /// <value>The cross sale APR (where applicable).</value>
        [DataMember(Name="APR")]
        public double? APR { get; set; }

        /// <summary>
        /// The term, in months, of the cross sale (where applicable).
        /// </summary>
        /// <value>The term, in months, of the cross sale (where applicable).</value>
        [DataMember(Name="Term")]
        public int? Term { get; set; }

        /// <summary>
        /// The cross sale facility (where applicable).
        /// </summary>
        /// <value>The cross sale facility (where applicable).</value>
        [DataMember(Name="Facility")]
        public string Facility { get; set; }

        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class CrossSale {\n");
            sb.Append("  Product: ").Append(Product).Append("\n");
            sb.Append("  ExpiryDate: ").Append(ExpiryDate).Append("\n");
            sb.Append("  Amount: ").Append(Amount).Append("\n");
            sb.Append("  APR: ").Append(APR).Append("\n");
            sb.Append("  Term: ").Append(Term).Append("\n");
            sb.Append("  Facility: ").Append(Facility).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }

        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

        /// <summary>
        /// Returns true if objects are equal
        /// </summary>
        /// <param name="obj">Object to be compared</param>
        /// <returns>Boolean</returns>
        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            return obj.GetType() == GetType() && Equals((CrossSale)obj);
        }

        /// <summary>
        /// Returns true if CrossSale instances are equal
        /// </summary>
        /// <param name="other">Instance of CrossSale to be compared</param>
        /// <returns>Boolean</returns>
        public bool Equals(CrossSale other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;

            return 
                (
                    Product == other.Product ||
                    Product != null &&
                    Product.Equals(other.Product)
                ) && 
                (
                    ExpiryDate == other.ExpiryDate ||
                    ExpiryDate != null &&
                    ExpiryDate.Equals(other.ExpiryDate)
                ) && 
                (
                    Amount == other.Amount ||
                    Amount != null &&
                    Amount.Equals(other.Amount)
                ) && 
                (
                    APR == other.APR ||
                    APR != null &&
                    APR.Equals(other.APR)
                ) && 
                (
                    Term == other.Term ||
                    Term != null &&
                    Term.Equals(other.Term)
                ) && 
                (
                    Facility == other.Facility ||
                    Facility != null &&
                    Facility.Equals(other.Facility)
                );
        }

        /// <summary>
        /// Gets the hash code
        /// </summary>
        /// <returns>Hash code</returns>
        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                var hashCode = 41;
                // Suitable nullity checks etc, of course :)
                    if (Product != null)
                    hashCode = hashCode * 59 + Product.GetHashCode();
                    if (ExpiryDate != null)
                    hashCode = hashCode * 59 + ExpiryDate.GetHashCode();
                    if (Amount != null)
                    hashCode = hashCode * 59 + Amount.GetHashCode();
                    if (APR != null)
                    hashCode = hashCode * 59 + APR.GetHashCode();
                    if (Term != null)
                    hashCode = hashCode * 59 + Term.GetHashCode();
                    if (Facility != null)
                    hashCode = hashCode * 59 + Facility.GetHashCode();
                return hashCode;
            }
        }

        #region Operators
        #pragma warning disable 1591

        public static bool operator ==(CrossSale left, CrossSale right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(CrossSale left, CrossSale right)
        {
            return !Equals(left, right);
        }

        #pragma warning restore 1591
        #endregion Operators
    }
}
